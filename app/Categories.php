<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $table = 'categories';

    protected $guarded = [];

    public function posts()
    {
        return $this->hasMany(Post::class);
    }
}
