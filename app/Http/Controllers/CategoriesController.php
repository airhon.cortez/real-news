<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;

class CategoriesController extends Controller
{
    public function categories() {

    	return view('categories.categories');
    }

    public function addCategory(Request $request){

    	$this -> validate($request, ['categories' => 'required']);

    	$categories = new Categories;
    	$categories ->categories =  $request->input('categories');
    	$categories -> save();
    	return redirect('/categories')-> with('response', 'Category Added Successfully');
    }
}
