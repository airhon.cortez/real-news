<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use App\Categories;
use App\Post;
use App\Likes;
use App\Dislikes;
use App\Comment;
use App\Profile;
use App\User;
use DB;
use Auth;



class PostController extends Controller
{
	public function post()
	{
		$categories = Categories::all();
		$posts = Post::get();
		return view('post.post', ['categories' => $categories, 'posts' => $posts]);
	}

	public function addPost(Request $request)
	{
		$this->validate($request, [
			'post_title' => 'required',
			'post_body' => 'required',
			'category_id' => 'required',
		]);

		$url = null;
		if ($request->hasFile('post_image')) {
			$file = $request->file('post_image');
			$file->move(
				public_path() . '/post_image',
				$file->getClientOriginalName()
			);
			$url = URL::to("/") . '/posts/' .
				$file->getClientOriginalName();
		}
		
		auth()->user()->posts()->create([
			'post_title' 	=> $request->post_title,
			'post_body' 	=> $request->post_body,
			'category_id' 	=> $request->category_id,
			'post_image' 	=> $url,
		]);

		return redirect('display')->with('response', 'Post added successfully');
	}

	public function view(Post $post)
	{
		return view('post.view', [
			'post' 			=> $post,
			'categories' 	=> Categories::get(),
		]);
	}

	public function edit(Post $post)
	{
		return view('post.edit', [
			'post' 			=> $post,
			'categories' 	=> Categories::get(),
		]);
	}

	public function editPost(Request $request, Post $post)
	{

		$this->validate($request, [
			'post_title' => 'required',
			'post_body' => 'required',
			'category_id' => 'required',
			'post_image' => 'required'
		]);

		$url = null;
		if ($request->hasFile('post_image')) {
			$file = $request->file('post_image');
			$file->move(
				public_path() . '/post_image',
				$file->getClientOriginalName()
			);
			$url = URL::to("/") . '/posts/' .
				$file->getClientOriginalName();
		}

		$post->update([
			'post_title' 	=> $request->post_title,
			'post_body' 	=> $request->post_body,
			'category_id' 	=> $request->category_id,
			'post_image'	=> $url,
		]);

		return redirect('post')->with('response', 'Post Updated successfully');
	}


	public function deletePost($post_id)
	{
		Post::where('id', $post_id)
			->delete();
		return redirect('/display')->with('response', 'Post Delete Succesfully');
	}

	public function category($cat_id)
	{
		$categories = Categories::all();
		$profiles = Profile::all();
		$like = Likes::all();
		$posts = DB::table('posts')
			->join('categories', 'posts.category_id', '=', 'categories.id')
			->join('profiles', 'posts.user_id', '=', 'profiles.id')
			->select('posts.*', 'categories.*', 'profiles.*')
			->where(['categories.id' => $cat_id])
			->get();

		return view('categories.categoriesposts', [
			'categories' => $categories,
			'profiles' => $profiles, 'posts' => $posts
		]);
	}

	public function like($id)
	{
		$loggedin_user = Auth::user()->id;
		$like_user = Likes::where(['user_id' => $loggedin_user, 'post_id' => $id])->first();
		if (empty($like_user->user_id)) {
			$user_id = Auth::user()->id;
			$email = Auth::user()->email;
			$post_id = $id;
			$like = new Likes;
			$like->user_id = $user_id;
			$like->email = $email;
			$like->post_id = $post_id;
			$like->save();
			return redirect("/view/{$id}");
		} else {
			return redirect("/view/{$id}");
		}
	}


	public function dislike($id)
	{
		$loggedin_user = Auth::user()->id;
		$like_user = Dislikes::where(['user_id' => $loggedin_user, 'post_id' => $id])->first();
		if (empty($like_user->user_id)) {
			$user_id = Auth::user()->id;
			$email = Auth::user()->email;
			$post_id = $id;
			$like = new Dislikes;
			$like->user_id = $user_id;
			$like->email = $email;
			$like->post_id = $post_id;
			$like->save();
			return redirect("/view/{$id}");
		} else {
			return redirect("/view/{$id}");
		}
	}


	public function comment(Request $request, $post_id)
	{

		$this->validate($request, [
			'comment' => 'required',
		]);

		$comment = new Comment;
		$comment->user_id = Auth::user()->id;
		$comment->post_id = $post_id;
		$comment->comment = $request->input('comment');
		$comment->save();
		return redirect("/view/{$post_id}")->with('response', 'Comment Added Succesfully');
	}
}
