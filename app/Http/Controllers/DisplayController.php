<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Post;
use Illuminate\Http\Request;

class DisplayController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $posts = Post::orderByDesc('created_at')->paginate();
        $categories = Categories::get();

        return view('displayprofile', [
            'posts'         => $posts,
            'categories'    => $categories
        ]);
    }
}
