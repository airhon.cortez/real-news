<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Profile;

class LoginController extends Controller
{


    use AuthenticatesUsers;


    public function redirectTo()
    {
        if(auth()->user()->profile) {
            return '/display';
        }

        return '/profile';
    }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
