<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
// Home Routes
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/profile_after/{id}', 'HomeController@profile_after');

// Post Routes
Route::get('/post', 'PostController@post');

Route:: get('/view/{post}', 'PostController@view')->name('post.view');

Route:: get('/edit/{post}', 'PostController@edit')->name('post.edit');

Route:: get('/editprofile/{id}', 'PostController@editprofile');

Route::get('/delete/{id}', 'PostController@deletePost');

Route::get('/category/{id}', 'PostController@category');

Route::get('/categ/{id}', 'PostController@categ');

Route:: get('/like/{id}', 'PostController@like');

Route:: get('/dislike/{id}', 'PostController@dislike');

Route:: post('/addPost', 'PostController@addPost');

Route:: post('/editPost/{post}' , 'PostController@editPost')->name('update.post');

Route:: post('/comment/{id}' , 'PostController@comment');

// Categories Routes
Route::get('/categories', 'CategoriesController@categories');

Route:: post('/addCategory', 'CategoriesController@addCategory');

// Profile Routes
Route::get('/profile', 'ProfileController@profile');

Route:: get('/displayarticle', 'ProfileController@article');

Route::	post('/addProfile', 'ProfileController@addProfile');

// Display Routes
Route::get('/display', 'DisplayController@index');

Route::get('/newsrticledisplay', 'Display@newsarticle');

// UserProfile Routes

Route::get('/userprofile', 'UserprofileController@index');

Route::get('/prof_display/{id}', 'UserprofileController@prof')->name('prof');

// About Routes
Route::get('/about', 'AboutController@about')->name('about');













