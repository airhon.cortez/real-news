<?php

use App\Categories;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    public function run()
    {
        $categories = [
            'Health',
            'Political',
            'Work and Education',
            'Technology and Entertainment',
            'Others',
        ];

        foreach ($categories as $category) {
            Categories::create([
                'categories' => $category
            ]);
        }
    }
}
