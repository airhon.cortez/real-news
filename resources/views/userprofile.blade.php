@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <link href="{{asset('fontawesome-free-5.13.1-web/css/fontawesome.css')}}" rel="stylesheet">
                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
                <script src='https://kit.fontawesome.com/a076d05399.js'></script>
                <div class="card-header">User Profile</div>

                <div class="card-body">
                    @if (count(auth()->user()->posts) > 0)
                        @foreach ( auth()->user()->posts as $post)
                            {{ $post }}
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
