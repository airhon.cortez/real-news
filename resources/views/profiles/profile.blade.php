@extends('layouts.app')

@section('content')

<style>
    body {
        margin:0;
        padding:0;
    }

    .profileadd {
        position:absolute;
        top:60%;
        left:50%;
        transform:translate(-50%, -50%);
        width:350px;
        height:420px;
        padding:80px 40px;
        box-sizing: border-box;
        background:#ADD8E6;
        
    }

    .heads {
        margin:0;
        padding: 0 0 20px;
        text-align:center;
    }

    .nick {
        padding: 0;
        margin: 0;
        font-weight:bold;
        color:$fff;
    }
    .des {
        padding: 0;
        margin: 0;
        font-weight:bold;
        color:$fff;
        }

        .profileadd input[type="text"], .profileadd input[type="description"] {
            border:none;
            border-bottom: 1px solid red;
            background: transparent;
            outline: none;
            height:40px;
            color:#fff;
            font-size:16px;
        }

        

        .profileadd input {
            width:100%;
            margin-bottom:20px;
        }

        .picture {
            width:100px;
            height:100px;
            overflow:hidden;
            position:absolute;
            top:calc(-100px/2);
            left:120px;
            border-radius:50%;
        }


        .container {
            position:absolute;
            top:85%;
            left:60%;
            transform: translate(-50%,-50%);
        }

        .submit {
            position:relative;
            text-align:center;
            width:250px;
            padding:5px;
            font-size:10px;
            color: black;
            font-family: poppins;
            font-weight:400;
            border: 2px solid #15f4ee;
            text-transform: uppercase;
            letter-spacing:15px;
            cursor:pointer;
            border-radius: 100px;
            transition:0.5s;
        }
        .submit:hover {
            box-shadow: 0 5px 50px 0 #2F4F4F inset, 0 5px 50px 0 #2F4F4F,
                        0 5px 50px 0 #2F4F4F inset, 0 5px 50px 0 #2F4F4F;
        }


</style>

<link href="{{asset('fontawesome-free-5.13.1-web/css/fontawesome.css')}}" rel="stylesheet">
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
            <script src='https://kit.fontawesome.com/a076d05399.js'></script>

        @if(count($errors) > 0)
            @foreach($errors -> all() as $error)
                <div class="alert alert-danger">{{$error}}</div>
            @endforeach
        @endif

            @if(session('response'))
            <div class="alert alert-success">{{session('response')}}</div>
            @endif
        
        <br>
        <br>

                       
        <div class="profileadd">
            <img src="/images/pro.gif" alt="" class="picture">
            <form method="POST" action='{{ url("/addProfile") }}' enctype="multipart/form-data">
                    @csrf
                    <p class="heads">Add User Description</p>
                    <form >
                    <p class="nick">Nickname</p>
                    <input placeholder="Place your Nickname" class="name"
                    id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" 
                    required autocomplete="name" autofocus>

                    @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror


                    <p class="des">Description</p>
                    <input name="designation"  class="designation" placeholder="Place your Bio"
                    id="designation" type="text" class="form-control @error('designation') is-invalid @enderror"
                        value="{{ old('designation') }}" required autocomplete="designation" autofocus>

                    @error('designation')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                    <input type="file" 
                    id="profile_pic" type="file" class="form-control-file" name="profile_pic" value="{{ old('profile_pic') }}" required autocomplete="profile_pic" autofocus>

                    @error('profile_pic')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            <div class="container">
                    <button type="submit" class="submit"> Add Profile</button>
                </div>
            </form>
        </div>                
                    </form>

                            </div>
                        </div>
                    </form>

            </div>
        </div>
        </div>
    </div>
</div>
@endsection
